package com.Zenya.Spooktober.Events;

import java.util.Random;

import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;

import com.Zenya.Spooktober.Items.Souls;

public class CustomDeathEvent extends Event implements Cancellable {
	
	private final EntityDeathEvent event;
	Souls souls = new Souls();
	
	public CustomDeathEvent(EntityDeathEvent event) {
		this.event = event;
		this.onCustomDeathEvent();
	}
	
	public void onCustomDeathEvent() {
		LivingEntity target = event.getEntity();
		if(!(target.hasMetadata("Spooktober"))) return;
		event.getDrops().clear();
		event.setDroppedExp(250);
		
		Random rObj = new Random();
		if(rObj.nextInt(5) >= 1) return;
		
		switch(target.getType()) {
		
		case ZOMBIE: {
			ItemStack drop = souls.get("LIGHT");
			event.getDrops().add(drop);
			break;
		}
		
		case SKELETON: {
			ItemStack drop = souls.get("NIGHT");
			event.getDrops().add(drop);
			break;
		}
		
		default:
			return;
		}
	}
	
	private static final HandlerList Handler = new HandlerList();
	@SuppressWarnings("unused")
	private boolean isCancelled;

	@Override
	public HandlerList getHandlers() {
		return Handler;
	}
	
	public static HandlerList getHandlerList() {
		return Handler;
	}
	
	public boolean isCancelled() {
		return this.isCancelled();
	}
	
	public void setCancelled(boolean isCancelled) {
		this.isCancelled = isCancelled;
	}


}

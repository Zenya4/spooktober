package com.Zenya.Spooktober.Events;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityTargetEvent;

public class Listeners implements Listener {
	
	private final boolean eventEnabled;
	
	public Listeners(boolean eventEnabled) {
		this.eventEnabled = eventEnabled;
	}
	
	@EventHandler
	public void onEntityDeathEvent(EntityDeathEvent event) {
		CustomDeathEvent cde = new CustomDeathEvent(event);
		Bukkit.getPluginManager().callEvent(cde);
	}
	
	@EventHandler
	public void onEntityTargetEvent(EntityTargetEvent event) {
		Entity entity = event.getEntity();
		if(!(entity.hasMetadata("Spooktober"))) return;
		
		if(!(event.getTarget() instanceof Player)) {
			event.setCancelled(true); // Stop Doots from targeting each other or targeting Spooks
			return;
		}
	}
	
	@EventHandler
	public void onEntityDamageByEntityEvent(EntityDamageByEntityEvent event) {
		Entity entity = event.getEntity();
		
		if(!(entity.hasMetadata("Spooktober"))) return;
		if(!(event.getDamager() instanceof Player)) return;
		
		Player player = (Player) event.getDamager();
		
		if(player.isFlying()) {
			player.sendMessage(entity.getCustomName() + " has reeled you in with a hook");
			player.teleport(entity);
			player.setFlying(false);
			player.setHealth(player.getHealth() - (double) 4);
			event.setCancelled(true);
		}
		
		Material ground = entity.getLocation().getBlock().getBlockData().getMaterial();
		if(ground == Material.WATER || ground == Material.LAVA) {
			player.sendMessage(entity.getCustomName() + " is invulnurable in liquids");
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onBlockBreakEvent(BlockBreakEvent event) {
		if(!(eventEnabled)) return;
		
		CustomItemDropEvent cide = new CustomItemDropEvent(event, eventEnabled);
		Bukkit.getPluginManager().callEvent(cide);
		event.setDropItems(false);
	}
}

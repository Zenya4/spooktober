package com.Zenya.Spooktober.Events;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class CustomSpawnEvent extends Event implements Cancellable {
	
	private final Plugin plugin;
	private final Location location;
	private final String type;
	
	
	public CustomSpawnEvent(Plugin plugin, Location location, String type) {
		this.plugin = plugin;
		this.location = location;
		this.type = type.toUpperCase();
		this.onCustomSpawnEvent();
	}
	
	public void onCustomSpawnEvent() {
		switch(this.type) {
		
		case "SPOOK": { // Spawn Zombie
			LivingEntity zombie = (LivingEntity) location.getWorld().spawnEntity(location, EntityType.ZOMBIE);
			
			PotionEffect fireResistanceEffect = new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 999999, 0, false, false);
			PotionEffect invisibilityEffect = new PotionEffect(PotionEffectType.INVISIBILITY, 999999, 0, false, false);
			PotionEffect resistanceEffect = new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 999999, 3, false, false);
			PotionEffect speedEffect = new PotionEffect(PotionEffectType.SPEED, 999999, 4, false, false);
			PotionEffect strengthEffect = new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 999999, 2, false, false); //1
			Collection<PotionEffect> potionEffects = new ArrayList<PotionEffect>(Arrays.asList(
					fireResistanceEffect, 
					invisibilityEffect, 
					resistanceEffect, 
					speedEffect, 
					strengthEffect));
			zombie.addPotionEffects(potionEffects);
			
			AttributeInstance healthAttribute = zombie.getAttribute(Attribute.GENERIC_MAX_HEALTH);
			AttributeInstance knockbackAttribute = zombie.getAttribute(Attribute.GENERIC_KNOCKBACK_RESISTANCE);
			AttributeInstance followRange = zombie.getAttribute(Attribute.GENERIC_FOLLOW_RANGE);
			healthAttribute.setBaseValue((double) 100);
			knockbackAttribute.setBaseValue((double) 0.25); 
			followRange.setBaseValue((double) 100);
			
			ItemStack pumpkin = new ItemStack(Material.JACK_O_LANTERN);
			zombie.getEquipment().setHelmet(pumpkin);
			
			zombie.setMetadata("Spooktober", new FixedMetadataValue(this.plugin, this.type));
			zombie.setCustomName(ChatColor.DARK_RED + "Spook");
			zombie.setFireTicks(999999);
			zombie.setHealth(100);
			zombie.setCanPickupItems(false);
			break;
		}
			
		case "DOOT": { // Spawn Skeleton
			LivingEntity skeleton = (LivingEntity) location.getWorld().spawnEntity(location, EntityType.SKELETON);
			
			PotionEffect fireResistanceEffect = new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 999999, 0, false, false);
			PotionEffect resistanceEffect = new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 999999, 2, false, false);
			PotionEffect speedEffect = new PotionEffect(PotionEffectType.SPEED, 999999, 6, false, false);
			Collection<PotionEffect> potionEffects = new ArrayList<PotionEffect>(Arrays.asList(
					fireResistanceEffect,  
					resistanceEffect, 
					speedEffect));
			skeleton.addPotionEffects(potionEffects);
			
			AttributeInstance healthAttribute = skeleton.getAttribute(Attribute.GENERIC_MAX_HEALTH);
			AttributeInstance followRange = skeleton.getAttribute(Attribute.GENERIC_FOLLOW_RANGE);
			healthAttribute.setBaseValue((double) 50); 
			followRange.setBaseValue((double) 100);
			
			ItemStack golden_helmet = new ItemStack(Material.GOLDEN_HELMET);
			ItemStack bow = new ItemStack(Material.BOW);
			bow.addUnsafeEnchantment(Enchantment.ARROW_DAMAGE, 16); //9, 7
			bow.addUnsafeEnchantment(Enchantment.ARROW_FIRE, 16960);
			bow.addUnsafeEnchantment(Enchantment.ARROW_KNOCKBACK, 5);
			skeleton.getEquipment().setHelmet(golden_helmet);
			skeleton.getEquipment().setItemInMainHand(bow);
			
			skeleton.setMetadata("Spooktober", new FixedMetadataValue(this.plugin, this.type));
			skeleton.setCustomName(ChatColor.DARK_PURPLE + "Doot");
			skeleton.setFireTicks(999999);
			skeleton.setHealth(50);
			skeleton.setCanPickupItems(false);
			break;
			}
		}
	}
	
	private static final HandlerList Handler = new HandlerList();
	@SuppressWarnings("unused")
	private boolean isCancelled;

	@Override
	public HandlerList getHandlers() {
		return Handler;
	}
	
	public static HandlerList getHandlerList() {
		return Handler;
	}
	
	public boolean isCancelled() {
		return this.isCancelled();
	}
	
	public void setCancelled(boolean isCancelled) {
		this.isCancelled = isCancelled;
	}

}

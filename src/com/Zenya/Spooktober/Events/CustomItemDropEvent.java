package com.Zenya.Spooktober.Events;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Random;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

public class CustomItemDropEvent extends Event implements Cancellable {
	
	private final BlockBreakEvent event;	
	Material[] materials = Material.values();
	ArrayList<String> bannedNames = new ArrayList<String>(
			Arrays.asList("BARRIER", 
					"BEDROCK",
					"COMMAND",
					"END_PORTAL",
					"SPAWN_EGG",
					"STRUCTURE"));
	
	public CustomItemDropEvent(BlockBreakEvent event, boolean eventEnabled) {
		this.event = event;
		
		if(eventEnabled) {
			this.onCustomItemDropEvent();
		}
	}
	
	public void onCustomItemDropEvent() {
		Collection<ItemStack> droppedItems = new ArrayList<ItemStack>();
		
		Player player = event.getPlayer();
		ItemStack handItem = player.getInventory().getItemInMainHand();
		droppedItems = event.getBlock().getDrops(handItem);
		
		Material randomMaterial = getRandomMaterial();
		
		for(int i = 0; i < droppedItems.size(); i++) {
			player.getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(randomMaterial));
		}
	}
	
	public Material getRandomMaterial() {
		Random rObj = new Random();
		int index = rObj.nextInt(materials.length);
		
		Material checkMaterial = materials[index];
		
		if(!(checkMaterial instanceof Material)) return getRandomMaterial();
		if(checkMaterial.equals(Material.AIR)) return getRandomMaterial();
		
		for(String bannedName : bannedNames) {
			if(checkMaterial.name().toUpperCase().contains(bannedName)) return getRandomMaterial();
		}
		
		return checkMaterial;
	}
	
	private static final HandlerList Handler = new HandlerList();
	@SuppressWarnings("unused")
	private boolean isCancelled;

	@Override
	public HandlerList getHandlers() {
		return Handler;
	}
	
	public static HandlerList getHandlerList() {
		return Handler;
	}
	
	public boolean isCancelled() {
		return this.isCancelled();
	}
	
	public void setCancelled(boolean isCancelled) {
		this.isCancelled = isCancelled;
	}


}

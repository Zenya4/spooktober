package com.Zenya.Spooktober.Scheduler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;

import com.Zenya.Spooktober.Events.CustomSpawnEvent;

public class SpawnManager {
	
	private final Plugin plugin;
	
	public SpawnManager(Plugin plugin) {
		this.plugin = plugin;
		this.runTask();
	}
	
	public void runTask() {
		BukkitScheduler scheduler = plugin.getServer().getScheduler();
		scheduler.scheduleSyncRepeatingTask(plugin, new Runnable() {
				@Override
				public void run() {
					
					if(Bukkit.getServer().getOnlinePlayers().size() != 0) {
					
					Player player = getRandomPlayer();
					
					if(getCanSpawnTime(player.getLocation().getWorld())) {
						Location spawnLocation = getSpawnLocation(player);
						if(spawnLocation != null) {
							spawnMobs(spawnLocation);
						}	
					}
				}
					despawnMobs();
			}
		}, 0L, 2400L);
	}
	
	public Player getRandomPlayer() {
		ArrayList<Player> playerList = new ArrayList<Player>();
		
		for(Player player : plugin.getServer().getOnlinePlayers()) {
			playerList.add(player);
		}
		
		Random rObj = new Random();
		int playerIndex = rObj.nextInt(playerList.size());
		
		return playerList.get(playerIndex);
	}
	
	public boolean getCanSpawnTime(World world) {
		Long time = world.getTime();
		
		if(time > 1000 && time < 13000) return false;
		
		return true;
	}
	
	public Location getSpawnLocation(Player player) {
		Location playerLocation = player.getLocation();
		double radius = 15;
		double height = 3;
		
		for(double x = playerLocation.getX() - radius; x < playerLocation.getBlockX() + radius; x++) {
			for(double y = playerLocation.getY() - height; y < playerLocation.getBlockY() + height; y++) {
				for(double z = playerLocation.getZ() - radius; z < playerLocation.getBlockZ() + radius; z++) {
					
					Block spawnBlock = player.getWorld().getBlockAt((int) x, (int) y, (int) z);
					Block spawnBlockUp = player.getWorld().getBlockAt((int) x, (int) y+1, (int) z);
					Block spawnBlockDown = player.getWorld().getBlockAt((int) x, (int) y-1, (int) z);
					
					if(spawnBlock.getLightLevel() < 8) {
						if(spawnBlock.getType() == Material.AIR && spawnBlockUp.getType() == Material.AIR) {
							if(spawnBlockDown.getType() != Material.AIR && 
									spawnBlockDown.getType() != Material.WATER && 
									spawnBlockDown.getType() != Material.LAVA) {
								
								return spawnBlock.getLocation();
							}
						}
					}
				}
			}
		}
		
		
		Location l = null;
		return l;
	}
	
	public void spawnMobs(Location location) {
		ArrayList<String> spawnTypes = new ArrayList<String>(Arrays.asList("SPOOK", "DOOT"));
		Random rObj = new Random();
		
		int spawnCount = rObj.nextInt(3) + 2;
		String spawnType = spawnTypes.get(rObj.nextInt(2));
		
		for(int i = 0; i < spawnCount; i++) {
			CustomSpawnEvent cse = new CustomSpawnEvent(plugin, location, spawnType);
			Bukkit.getPluginManager().callEvent(cse);
		}
	}
	
	public void despawnMobs() {
		for(World world: Bukkit.getServer().getWorlds()) {
			for(Entity entity : world.getLivingEntities()) {
				if(entity.hasMetadata("Spooktober") && (!getCanSpawnTime(entity.getWorld()))) {
					entity.remove();
				}
			}
		}
	}
}

package com.Zenya.Spooktober;

import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import com.Zenya.Spooktober.Commands.CommandManager;
import com.Zenya.Spooktober.Commands.PlayerCommands;
import com.Zenya.Spooktober.Events.Listeners;
import com.Zenya.Spooktober.Scheduler.SpawnManager;

public class Main extends JavaPlugin {
	
	public void onEnable() {
		Listener listener = new Listeners(false);
		this.getServer().getPluginManager().registerEvents(listener, this);
		
		this.getCommand("Spooktober").setExecutor(new CommandManager(this));
		this.getCommand("SpookyKey").setExecutor(new PlayerCommands());
		
		new SpawnManager(this);;
	}
	
	public void onDisable() {
		
	}

}

package com.Zenya.Spooktober.Commands;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.Zenya.Spooktober.Items.Souls;

public class PlayerCommands implements CommandExecutor {
	
	Souls souls = new Souls();

	@Override
	public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
		
		if(!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "This command can only be used by players");
			return true;
		}
		
		if(args.length != 0) return false;
		
		Player player = (Player) sender;
		ItemStack light = souls.get("LIGHT");
		ItemStack night = souls.get("NIGHT");
		List<String> lightLore = light.getItemMeta().getLore();
		List<String> nightLore = night.getItemMeta().getLore();
		boolean hasLight = false;
		boolean hasNight = false;
		
		for(ItemStack item : player.getInventory().getContents()) {
			if(!(item instanceof ItemStack)) continue;
			if(!(item.getItemMeta().hasLore())) continue;
			
			List<String> itemLore = item.getItemMeta().getLore();
			
			if(itemLore.equals(lightLore)) hasLight = true;
			if(itemLore.equals(nightLore)) hasNight = true;
		}
		
		if(hasLight && hasNight) {
			player.getInventory().removeItem(light);
			player.getInventory().removeItem(night);
			Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "crate give to " + player.getName() + " Spooky 1");
			player.sendMessage(ChatColor.GREEN + "You have crafted a Spooky Key");
		} else {
			player.sendMessage(ChatColor.RED + "You do not have the required materials to craft a Spooky Key");
		}
		
		return true;
	}

}

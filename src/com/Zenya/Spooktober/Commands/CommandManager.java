package com.Zenya.Spooktober.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import com.Zenya.Spooktober.Events.CustomSpawnEvent;
import com.Zenya.Spooktober.Events.Listeners;
import com.Zenya.Spooktober.Items.Souls;

public class CommandManager implements CommandExecutor {
	
	private final Plugin plugin;
	
	Souls souls = new Souls();
	
	boolean eventEnabled = false;
			
	public CommandManager(Plugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "This command can only be used by players");
			return true;
		}
		
		Player player = (Player) sender;
		
		if(args.length == 0) return false;
		
		switch(args[0]) {
		
			case "spawn": 
			case "spawnmob": {
					if(!(player.hasPermission("spooktober.spawn"))) {
						player.sendMessage(ChatColor.DARK_RED + "You do not have permission to use this command");
						return true;
					}
					
					if(args.length != 2) {
						player.sendMessage(ChatColor.RED + "Usage: /spooktober spawn [spook/doot]");
						return true;
					}
					
					String args1 = args[1].toUpperCase();
					String spawnType;
					
					if(args1.equals("SPOOK") || args1.equals("DOOT")) {
						spawnType = args1;
					} else {
						player.sendMessage(ChatColor.RED + "Usage: /spooktober spawn [spook/doot]");
						return true;
					}
					
					CustomSpawnEvent cse = new CustomSpawnEvent(plugin, player.getEyeLocation(), spawnType);
					Bukkit.getPluginManager().callEvent(cse);
					break;
			}
				
			case "getsoul":
			case "getsouls": {
					if(!(player.hasPermission("spooktober.getsoul"))) {
						player.sendMessage(ChatColor.DARK_RED + "You do not have permission to use this command");
						return true;
					}
					
					if(args.length != 2) {
						player.sendMessage(ChatColor.RED + "Usage: /spooktober getsoul [light/night]");
						return true;
					}
					
					String args1 = args[1].toUpperCase();
					String itemType;
					
					if(args1.equals("LIGHT") || args1.equals("NIGHT")) {
						itemType = args1;
					} else {
						player.sendMessage(ChatColor.RED + "Usage: /spooktober getsoul [light/night]");
						return true;
					}
					
					ItemStack drop = souls.get(itemType);
					player.getInventory().addItem(drop);
					break;
			}
				
			case "event":
			case "events": {
				if(!(player.hasPermission("spooktober.event"))) {
					player.sendMessage(ChatColor.DARK_RED + "You do not have permission to use this command");
					return true;
				}
				
				if(args.length != 1) {
					player.sendMessage(ChatColor.RED + "Usage: /spooktober event");
					return true;
				}
				
				if(eventEnabled) {
					eventEnabled = false;
				} else {
					eventEnabled = true;
				}
				
				player.sendMessage(ChatColor.DARK_RED + "Event Enabled: " + eventEnabled);
				HandlerList.unregisterAll(plugin);
				Listener listener = new Listeners(eventEnabled);
				Bukkit.getPluginManager().registerEvents(listener, plugin);
				break;
			}
			
		default:
			return false;
		}
		
		return true;
	}

}

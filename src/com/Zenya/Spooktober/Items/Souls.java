package com.Zenya.Spooktober.Items;

import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Souls {
	
	public ItemStack get(String type) {
		
		switch(type.toUpperCase()) {
		
		case "LIGHT": {
			ItemStack drop = new ItemStack(Material.BLAZE_POWDER);
			ItemMeta meta = drop.getItemMeta();
			meta.setDisplayName(ChatColor.DARK_RED + "Soul of Light");
			meta.setLore(Arrays.asList("", 
					ChatColor.RED + "Dropped by Spooks", 
					ChatColor.DARK_RED + "Used to craft Blessed gear",
					ChatColor.GOLD + "Valid in October 2019 only"));
			
			drop.setItemMeta(meta);
			drop.addUnsafeEnchantment(Enchantment.FIRE_ASPECT, 2);
			return drop;
		}
			
		case "NIGHT": {
			ItemStack drop = new ItemStack(Material.ENDER_EYE);
			ItemMeta meta = drop.getItemMeta();
			meta.setDisplayName(ChatColor.DARK_PURPLE + "Soul of Night");
			meta.setLore(Arrays.asList("", 
					ChatColor.LIGHT_PURPLE + "Dropped by Doots",
					ChatColor.DARK_PURPLE + "Used to craft Cursed gear",
					ChatColor.GOLD + "Valid in October 2019 only"));
			
			drop.setItemMeta(meta);
			drop.addUnsafeEnchantment(Enchantment.KNOCKBACK, 2);
			return drop;
		}
			
		default:
			return new ItemStack(Material.AIR);
		}
	}

}
